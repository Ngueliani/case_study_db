package com.ngueliani.case_study.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.ngueliani.case_study.model.AccessPoint;

@RestController
public class AccessController {
	
	@GetMapping("/betriebsstelle/{itemId}")
	public AccessPoint getAccesspoint (@PathVariable("itemId") String itemId) {
		
		AccessPoint accessPoint = new AccessPoint();
		accessPoint.setName("0");
		accessPoint.setShortname("0");
		accessPoint.setType("0");
		
		String path = "/Users/tchap/SpringToolSuite-workspace/case_study/DBNetz-Betriebsstellenverzeichnis-Stand2017-01.csv";
		String line = ""; 
		
		try {
			//reads the file
			BufferedReader br = new BufferedReader(new FileReader(path));
		
			while((line = br.readLine()) != null) {
				String [] accessPoints = line.split(";");
				
				if (accessPoints[0].equalsIgnoreCase(itemId)) {
					
					//saves the found data in the object 
					accessPoint.setName(accessPoints[1]);
					accessPoint.setShortname(accessPoints[2]);
					accessPoint.setType(accessPoints[3]);
					br.close();
					break;
				}		
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return accessPoint;
	}
	
}

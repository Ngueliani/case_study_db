package com.ngueliani.case_study.model;

public class AccessPoint {
	String name;
	String shortname;
	String type;
	
	public void setName (String name) {
		this.name = name;
	}
	
	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
	
	public void setType(String type) {
		this.type = type;
	}	
		
	public String getName() {
		return this.name;
	}
	
	public String getShortname() {
		return this.shortname;
	}
	
	public String getType() {
		return this.type;
	}
	
}

package com.ngueliani.case_study;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

//charges the spring context by the execution of the test
//makes objects of MockMvc available

@SpringBootTest
@AutoConfigureMockMvc
class CaseStudyApplicationTests {
	
	//Spring gives an instance of this object
	@Autowired
	public MockMvc mockMvc;

	//verifies the response
	@Test
	public void testGetAccesspoint() throws Exception {
		mockMvc.perform(get("/betriebsstelle/aamp")).andExpect(status().isOk());
	}

}
